'use strict';

console.log('HELLO WORLD\n JavaScript');
let a;
console.log(a);
a = 10;
console.log('Type of a:', typeof(a));

console.log(a);
a = 'New text';
console.log(a);
console.log('Type of a:', typeof(a));


//alert(a); // останавливает выполнение кода до нажатия на кнопку OK
// a = null в typeof ошибка общепризнанная. Вщзвращает объект

//------------------------ Операции с числами -----------------------------

let first = 3;
let second = 2;

let result;

result = first + second;
console.log('Результат =', result);

result = first % second;
console.log('Остаток от деления = ', result);

// при сложении если хоть один из операндов является строкой, то все приведется к строке. "Конкотенация"
// все остальные операции пытаются привести к числу

first = 'Hello';
second = 5;

result = first - second;
console.log('Результат =', result); // NaN

//----------------------------

let i = 0;
console.log('i=', i);
i = i + 1; // i +=1 , i++
console.log('i=', i);
++i;
console.log('i=', i);
i++;
console.log('i=', i);

//------------------------------------

let c = '35';
console.log('c=', c);
console.log('c=', +c); // преобразование к числу

let d = 35;
console.log(d + ''); // преобразование к строке, костыльный метод. Нормальный toString()

console.log('boolean:', !!d); // надо ставить 2 восклицательных знака для преобразования к булевому значению

//-------------------------------------------

let x = 'A';
let y = 'a'; // по таблице юникода

console.log('x > y:', x > y);

let z = '35';
let s = 35;

console.log('Без сравнения типов данных:', z==s);  // true
console.log('Со сравнением типов данных:', z===s); // false

//--------------------------------------------

// let age;
// age = prompt('Введите ваш возраст: '); // все приходит в виде строки. Синхронная операция
// console.log(age);
// console.log(age >= 18);


//--------------------------------------

// let isOld;

// isOld = confirm('Если вам 18 лет?'); // возвращает булевое знчение
// console.log(isOld);

//--------------------------------------
